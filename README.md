# varscan Singularity container
### Bionformatics package varscan<br>
variant detection in massively parallel sequencing data<br>
varscan Version: 2.4.4<br>
[http://dkoboldt.github.io/varscan/]

Singularity container based on the recipe: Singularity.varscan_v2.4.4

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build varscan_v2.4.4.sif Singularity.varscan_v2.4.4`

### Get image help
`singularity run-help ./varscan_v2.4.4.sif`

#### Default runscript: STAR
#### Usage:
  `varscan_v2.4.4.sif --help`<br>
    or:<br>
  `singularity exec varscan_v2.4.4.sif varscan --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull varscan_v2.4.4.sif oras://registry.forgemia.inra.fr/gafl/singularity/varscan/varscan:latest`


